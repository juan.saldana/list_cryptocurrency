# ListCryptocurrency

## To run this backend you need have instaled before:
    - Java SKD 8
    - MongoDB 4.4

# First 
## Start mongodb services and create superuser to connect correctly:
    - mongod --port 27017 --dbpath /data/db1
    - mongo --port 27017
    - use admin
    - db.createUser(
        {
            user: "myUserAdmin",
            pwd: "abc123",
            roles: [ { role: "userAdminAnyDatabase", db: "admin" } ]
        }
    )
    - exit
    - mongod --auth --port 27017 --dbpath /data/db1
    - mongo --port 27017 -u "myUserAdmin" -p "abc123" \ --authenticationDatabase "admin"

# Next
Clone the project and run on your IDE (IntelliJ IDEA or Eclipse), build and run Main Class.
