package com.example.list_cryptocurrency.services;

import com.example.list_cryptocurrency.models.Client;
import com.example.list_cryptocurrency.repositories.ClientRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@AllArgsConstructor
@Service
public class ClientService {
    private final ClientRepository repository;

    public List<Client> getAll(){
        return repository.findAll();
    }

    public Optional<Client> getOne(String id) {
        return repository.findById(id);
    }

    public Client create(Client client){
        try{
            return repository.insert(client);
        }catch (Exception e){
            return null;
        }
    }

    public Optional<Client> update(String id, Client client){
        Optional<Client> clientData = getOne(id);

        if (clientData.isPresent()){
            Client _client = clientData.get();
            _client.setLatitude(client.getLatitude());
            _client.setLongitude(client.getLongitude());
            _client.setIp(client.getIp());
            _client.setDevice(client.getDevice());
            _client.setSearch_date(client.getSearch_date());
            return Optional.of(repository.save(_client));
        }else {
            return Optional.empty();
        }
    }

    public String delete(String id){
        Optional<Client> clientData = getOne(id);

        if (clientData.isPresent()){
            repository.deleteById(id);
            return "Deleted client with id: " +id;
        }else {
            return "Don't exist client with id: " +id;
        }
    }

}
