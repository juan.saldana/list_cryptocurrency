package com.example.list_cryptocurrency.models;
import com.example.list_cryptocurrency.enums.Device;
import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

import java.time.LocalDateTime;

@Data
@Document
public class Client {
    @Id
    private String id;
    private String latitude;
    private String longitude;
    @Indexed(unique = true)
    private String ip;
    private Device device;
    private LocalDateTime search_date;

    public Client(String latitude, String longitude, String ip, Device device, LocalDateTime search_date) {
        this.latitude = latitude;
        this.longitude = longitude;
        this.ip = ip;
        this.device = device;
        this.search_date = search_date;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public Device getDevice() {
        return device;
    }

    public void setDevice(Device device) {
        this.device = device;
    }

    public LocalDateTime getSearch_date() {
        return search_date;
    }

    public void setSearch_date(LocalDateTime search_date) {
        this.search_date = search_date;
    }
}
