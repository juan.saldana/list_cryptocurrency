package com.example.list_cryptocurrency.controllers;

import com.example.list_cryptocurrency.services.ClientService;
import com.example.list_cryptocurrency.models.Client;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@CrossOrigin
@RequestMapping("clients")
@AllArgsConstructor
public class ClientController {
    private final ClientService clientService;

    @GetMapping
    public List<Client> getAll() {
        return clientService.getAll();
    }

    @GetMapping("/{id}")
    public Optional<Client> getOne(@PathVariable String id){
        return clientService.getOne(id);
    }

    @PostMapping
    public Client create(@RequestBody Client client) {
        return clientService.create(client);
    }

    @PutMapping("/{id}")
    public Optional<Client> update(@PathVariable String id, @RequestBody Client client){
        return clientService.update(id, client);
    }

    @DeleteMapping("/{id}")
    public String delete(@PathVariable String id){
        return clientService.delete(id);
    }
}
