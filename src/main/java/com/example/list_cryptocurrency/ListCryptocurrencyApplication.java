package com.example.list_cryptocurrency;

import com.example.list_cryptocurrency.models.Client;
import com.example.list_cryptocurrency.repositories.ClientRepository;
import com.example.list_cryptocurrency.enums.Device;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;

import java.time.LocalDateTime;
import java.util.List;

@SpringBootApplication
public class ListCryptocurrencyApplication {

	public static void main(String[] args) {
		SpringApplication.run(ListCryptocurrencyApplication.class, args);
	}

	@Bean
	CommandLineRunner runner(ClientRepository repository, MongoTemplate template){
		return arg -> {
			String ip = "186.147.177.15";
			Client client = new Client(
					"3.53944",
					"-76.30361",
					ip,
					Device.DESKTOP,
					LocalDateTime.now()
			);

			Query query = new Query().addCriteria(Criteria.where("ip").is(ip));
			List<Client> clients = template.find(query, Client.class);

			if (clients.isEmpty()) {
				repository.insert(client);
			}else {
				System.out.println("Already exists client");
			}
		};
	}

}

