package com.example.list_cryptocurrency.repositories;

import com.example.list_cryptocurrency.models.Client;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface ClientRepository extends MongoRepository<Client, String> { }
