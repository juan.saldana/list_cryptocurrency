package com.example.list_cryptocurrency.enums;

public enum Device {
    MOBILE, DESKTOP
}
